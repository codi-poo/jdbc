/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package conexion;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 *
 * @author jesus
 */
public class Conexion {

    private Connection conexion = null;

    private String servidor = "localhost:3306";
    private String database = "db_jdbc";
    private String usuario = "root";
    private String password = "08090385";

    public Conexion() {
        try {

            Class.forName("com.mysql.jdbc.Driver");
            String url = "jdbc:mysql://" + servidor + "/" + database + "?useSSL=false";
            conexion = DriverManager.getConnection(url, usuario, password);

        } catch (SQLException ex) {
            ex.printStackTrace();
        } catch (ClassNotFoundException ex) {
            ex.printStackTrace();
        }
    }

    public Connection getConexion() {
        return conexion;
    }

    public void setConexion(Connection conexion) {
        this.conexion = conexion;
    }

    public void closeConexion() throws SQLException {
        this.conexion.close();
    }

}
