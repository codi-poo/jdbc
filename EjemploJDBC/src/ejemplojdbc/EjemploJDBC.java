/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejemplojdbc;

import conexion.Conexion;
import java.sql.*;
import java.sql.Statement;

/**
 *
 * @author jesus
 */
public class EjemploJDBC {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        delete(1);
    }

    public static void insert(int id, String descripcion) {
        Conexion conn = null;
        String sql = "INSERT INTO tabla (id,descripcion) VALUES (?, ?)";
        try {
            conn = new Conexion();
            PreparedStatement statement = conn.getConexion().prepareStatement(sql);
            statement.setInt(1, id);
            statement.setString(2, descripcion);

            int rowsInserted = statement.executeUpdate();

            if (rowsInserted > 0) {
                System.out.println("Insertado con Exito!");
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            if (conn != null) {
                try {
                    conn.closeConexion();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        }
    }

    public static void seleted() {
        Conexion conn = null;
        String sql = "select * from tabla";
        try {
            conn = new Conexion();

            Statement statement = conn.getConexion().createStatement();
            ResultSet result = statement.executeQuery(sql);

            int count = 0;

            while (result.next()) {
                String id = result.getString(1);
                String descipcion = result.getString("descripcion");

                String output = "User #%d: %s - %s";
                System.out.println(String.format(output, ++count, id, descipcion));
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            if (conn != null) {
                try {
                    conn.closeConexion();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        }
    }

    public static void update(int id, String descripcion) {
        Conexion conn = null;
        String sql = "UPDATE tabla SET descripcion=? WHERE id=?";
        try {
            conn = new Conexion();
            PreparedStatement statement = conn.getConexion().prepareStatement(sql);
            statement.setString(1, descripcion);
            statement.setInt(2, id);

            int rowsUpdated = statement.executeUpdate();
            if (rowsUpdated > 0) {
                System.out.println("Actualizado con exito!");
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            if (conn != null) {
                try {
                    conn.closeConexion();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        }
    }

    public static void delete(int id) {
        Conexion conn = null;
        String sql = "DELETE FROM tabla WHERE id=?";

        try {
            conn = new Conexion();

            PreparedStatement statement = conn.getConexion().prepareStatement(sql);
            statement.setInt(1, id);

            int rowsDeleted = statement.executeUpdate();
            if (rowsDeleted > 0) {
                System.out.println("Eliminado con exito!");
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            if (conn != null) {
                try {
                    conn.closeConexion();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        }
    }

}
